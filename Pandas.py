#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
get_ipython().system('{sys.executable} -m pip install pandas')
'pip install --upgrade pip'


# # Quiz Questions

# In[10]:


import pandas as pd
arr = [[1,2],[4,5],[7,8]]
df = pd.DataFrame(arr,index=['a','b','c'],columns=['A','B'])
df
df.loc[:,'A']


# In[12]:


one = ['one',1,'one1']
a = ['A','a','Aa']
df1 = pd.DataFrame({'one': one, 'a': a}) 
df1 = df1[['one', 'a']] 
a = ['A','a','Aa'] 
b = ['B','b','Bb']
df2 = pd.DataFrame({'a': a, 'b': b}) 
df2 = df2[['a', 'b']]
df1.merge(df2, on='a', how='inner')


# # Assignment Questions

# In[5]:


import pandas as pd
data = pd.read_csv('Desktop//IQ_scores.txt', delimiter = "\t")
print(data)


# In[6]:


data1 = data.sort_values(by='IQ')
print(data1)


# In[7]:


Row_list =[]
for i in range((data1.shape[0])):
    Row_list.append(list(data1.iloc[i, :]))
    print(Row_list)


# In[17]:


data1.info(null_counts=True)


# In[23]:


data1.mean()


# In[22]:


data1["Brain"].mean()


# In[25]:


data1[["Brain"]]


# # My data file

# In[43]:


import sys
get_ipython().system('{sys.executable} -m pip install xlrd')
'pip install --upgrade pip'


# In[1]:


import sys
get_ipython().system('{sys.executable} -m pip install seaborn')
'pip install --upgrade pip'


# In[2]:


'pip install --upgrade pip'


# In[1]:


import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import xlrd
import seaborn as sn


# In[11]:


HBA_SCN2 = pd.read_excel('Desktop//4-hydroxybenzaldehyde_New.xls', sheet_name='SCN2_Raw_Data')
print(HBA_SCN2)


# In[17]:


HBA_SCN2_1 = HBA_SCN2.drop('Raw Data',axis=1)
# print(HBA_SCN2_1)
HBA_SCN2_2 = HBA_SCN2_1.drop('Samples',axis=1)
# print(HBA_SCN2_2)


# In[73]:


print(HBA_SCN2_1)


# In[87]:


S54_3_T = HBA_SCN2_1[0:5]
print(S54_3_T)
S54_3_T1 = S54_3_T.drop("Samples",axis=1)
print(S54_3_T1)
S54_3_T.mean()


# In[88]:


list1 = S54_3_T1
print(list1)


# In[18]:


print(HBA_SCN2_2)


# In[22]:


first_five = (HBA_SCN2_2[0:5])
print(first_five)


# In[23]:


S54_3 = first_five.transpose()
print(S54_3)


# In[33]:


second_five = (HBA_SCN2_2[5:11])
print(second_five)


# In[34]:


S54_5 = second_five.transpose()
print(S54_5)


# In[24]:


import numpy as np


# In[27]:


list1 = S54_3.to_numpy().tolist()
print(list1)
print(type(list1))


# In[35]:


list2 = S54_5.to_numpy().tolist()
print(list2)


# In[41]:


pd.read_excel('Desktop//4-hydroxybenzaldehyde_New.xls', sheet_name='Sheet1')


# In[40]:


boxplot = HBA_1.boxplot()


# In[71]:


HBA_SCN2 = pd.read_excel('Desktop//4-hydroxybenzaldehyde_New.xls', sheet_name='SCN2_Raw_Data')
print(HBA_SCN2)


# In[ ]:




